package com.yurazaluskyi.view;

import com.yurazaluskyi.controller.Controller;

import java.util.Scanner;

public class View {

    private Controller controller = new Controller();
    private Scanner sc = new Scanner(System.in);

    private void menu() {
        System.out.println("1 - print departments of hypermarket");
        System.out.println("2 - print section of department");
        System.out.println("3 - print commodities of section");
        System.out.println("4 - add department to hypermarket");
        System.out.println("5 - add section to department");
        System.out.println("6 - add commodity to hypermarket");
        System.out.println("7 - print commodities by price within min and max");
        System.out.println("E X I T - enter any number");
    }

    public void show() {
        while (true) {
            menu();
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    controller.printDepartmentsHypermarket();
                    break;
                case 2:
                    controller.printSectionsOfDepartment();
                    break;
                case 3:
                    controller.printCommoditiesOfSection();
                    break;
                case 4:
                    controller.addDepartmentToHypermarket();
                    break;
                case 5:
                    controller.addSectionToDepartment();
                    break;
                case 6:
                    controller.addCommodityToHypermarket();
                    break;
                case 7:
                    controller.printCommodityWithinPriceMaxMin();
                    break;
                default:
                    System.out.println("Good luck!");
                    return;
            }
        }
    }
}
