package com.yurazaluskyi.model;

import java.util.ArrayList;
import java.util.List;

public class Hypermarket {
    private List<Department> listDepartments = new ArrayList<Department>();

    public Hypermarket() {
    }

    public Hypermarket(List<Department> listDepartments) {
        this.listDepartments = listDepartments;
    }

    public List<Department> getListDepartments() {
        return listDepartments;
    }

    public void setListDepartments(List<Department> listDepartments) {
        this.listDepartments = listDepartments;
    }

    //Methods////////////////////////////////////////////////////////////////////////////////////////////////

    public void addDepartmentToHypermarket(Department department) {
        listDepartments.add(department);
    }

    public void printListDepartments() {
        for (Department department : listDepartments) {
            System.out.println("id - " + department.getId() + "    " + "name - " + department.getTitle());
        }
    }

    public void deleteDepartmentFromHypermarket(Department department) {
        listDepartments.remove(department);
    }

    public Department getDepartmentById(int id) {
        for (Department department : listDepartments) {
            if (department.getId() == id) {
                return department;
            }
        }
        return null;
    }

    public Department getDepartmentByTitle(String title) {
        title = title.toLowerCase();
        for (Department department : listDepartments) {
            if (department.getTitle().toLowerCase().equals(title)) {
                return department;
            }
        }
        return null;
    }

    public Section getSectionByTitle(String titleSection) {
        for (Department department : listDepartments) {
            for (Section section : department.getListSections()) {
                if(section.getTitle().equals(titleSection)){
                    return section;
                }
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Hypermarket{" +
                "listDepartments=" + listDepartments +
                '}';
    }
}
