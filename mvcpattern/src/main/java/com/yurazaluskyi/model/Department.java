package com.yurazaluskyi.model;

import java.util.ArrayList;
import java.util.List;

public class Department {
    private int id;
    private String title;
    private List<Section> listSections = new ArrayList<Section>();

    public Department() {
    }

    public Department(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public Department(int id, String title, List<Section> listSections) {
        this.id = id;
        this.title = title;
        this.listSections = listSections;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Section> getListSections() {
        return listSections;
    }

    public void setListSections(List<Section> listSections) {
        this.listSections = listSections;
    }

    //METHODS//////////////////////////////////////////////////////////////////////////////////////////////

    public void addSectionToDepartment(Section section) {
        listSections.add(section);
    }

    public void printListSection() {
        for (Section section : listSections) {
            System.out.println("id - " + section.getId() + "    " + "title - " + section.getTitle());
        }
    }

    public void deleteSectionFromDepartment(Section section) {
        listSections.remove(section);
    }

    public Section getSectionById(int id) {
        for (Section section : listSections) {
            if (section.getId() == id) {
                return section;
            }
        }
        return null;
    }

    public Section getSectionByTitle(String title) {
        title = title.toLowerCase();
        for (Section section : listSections) {
            if (section.getTitle().toLowerCase().equals(title)) {
                return section;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", listSections=" + listSections +
                '}';
    }
}
