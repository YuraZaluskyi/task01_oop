package com.yurazaluskyi.model;

import java.util.ArrayList;
import java.util.List;

public class Section {
    private int id;
    private String title;
    private List<Commodity> listCommodities = new ArrayList<Commodity>();

    public Section() {
    }

    public Section(int id, String name) {
        this.id = id;
        this.title = name;
    }

    public Section(int id, String name, List<Commodity> listCommodities) {
        this.id = id;
        this.title = name;
        this.listCommodities = listCommodities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Commodity> getListCommodities() {
        return listCommodities;
    }

    public void setListCommodities(List<Commodity> listCommodities) {
        this.listCommodities = listCommodities;
    }

    //METHODS////////////////////////////////////////////////////////////////////////////////////////////////

    public void addCommodityToSection(Commodity commodity) {
        listCommodities.add(commodity);
    }

    public void printListComodities() {
        for (Commodity commodity : listCommodities) {
            System.out.println("id - " + commodity.getId() + "    " + "title - " + commodity.getTitle() +
                    "price - " + commodity.getPrice());
        }
    }

    public void deleteCommodityFromSection(Commodity commodity) {
        listCommodities.remove(commodity);
    }

    public Commodity getCommodityById(int id) {
        for (Commodity commodity : listCommodities) {
            if (commodity.getId() == id) {
                return commodity;
            }
        }
        return null;
    }

    public Commodity getCommodityByTitle(String title) {
        title = title.toLowerCase();
        for (Commodity commodity : listCommodities) {
            if (commodity.getTitle().toLowerCase().equals(title)) {
                return commodity;
            }
        }
        return null;
    }

    public List<Commodity> getCommoditiesByPriceWithinMaxMin(int min, int max) {
        List<Commodity> commodities = new ArrayList<Commodity>();
        for (Commodity commodity : listCommodities) {
            if (commodity.getPrice() >= min && commodity.getPrice() <= max) {
                commodities.add(commodity);
            }
        }
        return commodities;
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", listCommodities=" + listCommodities +
                '}';
    }
}
