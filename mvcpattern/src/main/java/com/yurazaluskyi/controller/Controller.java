package com.yurazaluskyi.controller;

import com.yurazaluskyi.model.Commodity;
import com.yurazaluskyi.model.Department;
import com.yurazaluskyi.model.Hypermarket;
import com.yurazaluskyi.model.Section;

import java.util.Scanner;

public class Controller {

    private Hypermarket hypermarket = new Hypermarket();
    private Scanner input = new Scanner(System.in);

    public Controller() {

    }

    public void printDepartmentsHypermarket() {
        hypermarket.printListDepartments();
    }

    public void printSectionsOfDepartment() {
        printDepartmentsHypermarket();
        System.out.println("Enter the title of department");
        String titleDepartment = input.next();
        Department department = hypermarket.getDepartmentByTitle(titleDepartment);
        department.printListSection();
    }

    public void printCommoditiesOfSection() {
        printDepartmentsHypermarket();
        System.out.println("Enter the title section");
        String titleSection = input.next();
        hypermarket.getSectionByTitle(titleSection);
    }

    public void addDepartmentToHypermarket() {
        System.out.println("Enter id of department");
        int id = input.nextInt();
        System.out.println("Enter title of department");
        String title = input.next();
        title = title.toLowerCase();
        hypermarket.addDepartmentToHypermarket(new Department(id, title));
    }

    public void addSectionToDepartment() {
        System.out.println("Enter the title of the department to which you want to add the section");
        String titleDepartment = input.next();
        System.out.println("Enter id of section");
        int id = input.nextInt();
        System.out.println("Enter title of section");
        String titleSection = input.next();
        Department department = hypermarket.getDepartmentByTitle(titleDepartment);
        department.addSectionToDepartment(new Section(id, titleSection));
    }

    public void addCommodityToHypermarket() {
        hypermarket.printListDepartments();
        System.out.println("Enter the title of the department to which you want to add the commodity");
        String titleDepartment = input.next();
        Department department = hypermarket.getDepartmentByTitle(titleDepartment);
        department.printListSection();
        System.out.println("Enter the title of the section to which you want to add the commodity");
        String titleSection = input.next();
        Section section = hypermarket.getSectionByTitle(titleSection);
        System.out.println("Enter id of the commodity");
        int id = input.nextInt();
        System.out.println("Enter name of commodity");
        String titleCommodity = input.next();
        titleCommodity = titleCommodity.toLowerCase();
        System.out.println("Enter price of commodity");
        int priceCommodity = input.nextInt();
        section.addCommodityToSection(new Commodity(id, priceCommodity, titleCommodity));
    }

    public void printCommodityWithinPriceMaxMin() {
        hypermarket.printListDepartments();
        System.out.println("Enter the title of Department");
        String titleDepartment = input.next();
        Department department = hypermarket.getDepartmentByTitle(titleDepartment);
        department.printListSection();
        System.out.println("Enter the title of section");
        String titleSection = input.next();
        Section section = department.getSectionByTitle(titleSection);
        System.out.println("Enter min price");
        int minPrice = input.nextInt();
        System.out.println("Enter max price");
        int maxPrice = input.nextInt();
        System.out.println(section.getCommoditiesByPriceWithinMaxMin(minPrice, maxPrice));
    }
}
