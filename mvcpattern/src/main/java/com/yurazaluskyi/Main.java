package com.yurazaluskyi;

import com.yurazaluskyi.view.View;

public class Main {
    public static void main(String[] args) {
        View view = new View();
        view.show();
    }
}
